import numpy as np
import sim

from utils import *

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

if clientID != -1:
  print ("Connected to remote API server")

  _, floor = sim.simxGetObjectHandle(clientID, 'ResizableFloor_5_25', sim.simx_opmode_oneshot_wait)

  # Handle para o ROBÔ    
  robotname = 'Pioneer_p3dx'
  returnCode, robotHandle = sim.simxGetObjectHandle(clientID, robotname, sim.simx_opmode_oneshot_wait)       
  
  # Handle para os dados do LASER
  laser_data_name = "hokuyo_range_data"
  
  # Geralmente a primeira leitura é inválida (atenção ao Operation Mode)
  # Em loop até garantir que as leituras serão válidas
  returnCode = 1
  while returnCode != 0:
    returnCode, range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_streaming + 10)
  
  # Prosseguindo com as leituras
  returnCode, string_range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_buffer)
  raw_range_data = sim.simxUnpackFloats(string_range_data)
  
  scan_range = 180 * np.pi / 180
  step_size = 2 * np.pi / 1024
  laser_data = format_laser_data(raw_range_data, scan_range, step_size)

  returnCode = 1
  while returnCode != 0:
    # Encontra a posição do robô em relação ao mundo
    returnCode, robot_t = sim.simxGetObjectPosition(clientID, robotHandle, -1, sim.simx_opmode_streaming)

    # Encontra a rotação relativa do chão em relação ao robô
    returnCode, floor_r = sim.simxGetObjectOrientation(clientID, floor, robotHandle, sim.simx_opmode_streaming)

  # Cria a matriz de transformação
  floor_m = create_matrix(robot_t, Rz(floor_r[2]))

  # Desenha os pontos que o laser do robô vê transladados pela matriz criada
  draw_laser_data(laser_data, floor_m) 
    
  sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)         
  sim.simxFinish(clientID)
else:
  print ("Failed connecting to remote API server")
    
print ("Program ended")