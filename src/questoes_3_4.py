import sim
import time
import matplotlib.pyplot as plt

from utils import *

robot_name = "Pioneer_p3dx"
object_names = ["Bill", "diningTable", "sofa", "slidingDoor", "indoorPlant"]
colors = ["red", "green", "blue", "orange", "purple"]

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

if clientID != -1:
  print ("Connected to remote API server")

  robot_handle = sim.simxGetObjectHandle(clientID, robot_name, sim.simx_opmode_oneshot_wait)[1]
  handles = [sim.simxGetObjectHandle(clientID, name, sim.simx_opmode_oneshot_wait)[1] for name in object_names]

  # Leia até que o resultado não seja um erro
  returnCode = 1
  while returnCode != 0:
    positions = [sim.simxGetObjectPosition(clientID, h, robot_handle, sim.simx_opmode_streaming) for h in handles]
    rotations = [sim.simxGetObjectOrientation(clientID, h, robot_handle, sim.simx_opmode_streaming)[1] for h in handles]
    returnCode = positions[0][0]
    positions = [p[1] for p in positions]

  # Printa a informação de posicão e rotação relativa de cada objeto
  for obj_name, pos, rot in zip(object_names, positions, rotations):
    print(obj_name, pos, rot[2])

  # Desenha as ligações dos sistemas de coordenada
  for pos in positions:
    plt.plot([0, pos[0]], [0, pos[1]], c='black')

  # Desenha os frames
  for pos, rot, color in zip(positions, rotations, colors):
    plot_frame(pos, Rz(rot[2]), c=[color, color])
 
  # Ajusta os eixos do plot
  plt.xlim(-4, 4)
  plt.ylim(-4, 4)
  plt.gca().set_aspect('equal', adjustable='box')
  plt.show()

  sim.simxGetPingTime(clientID)
  sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)         
  sim.simxFinish(clientID)
else:
  print ("Failed connecting to remote API server")

print ("Program ended")