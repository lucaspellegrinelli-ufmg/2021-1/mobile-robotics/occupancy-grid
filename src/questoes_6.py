import numpy as np
import matplotlib.pyplot as plt
import sim
import time

from utils import *

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

if clientID != -1:
  print ("Connected to remote API server")

  # Handle para o ROBÔ    
  robotname = "Pioneer_p3dx"
  returnCode, robotHandle = sim.simxGetObjectHandle(clientID, robotname, sim.simx_opmode_oneshot_wait)     
  
  # Handle para as juntas das RODAS
  returnCode, l_wheel = sim.simxGetObjectHandle(clientID, robotname + "_leftMotor", sim.simx_opmode_oneshot_wait)
  returnCode, r_wheel = sim.simxGetObjectHandle(clientID, robotname + "_rightMotor", sim.simx_opmode_oneshot_wait)    
  
  # Handle para os dados do LASER
  laser_data_name = "hokuyo_range_data"
  
  # Geralmente a primeira leitura é inválida (atenção ao Operation Mode)
  # Em loop até garantir que as leituras serão válidas
  returnCode = 1
  while returnCode != 0:
    returnCode, range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_streaming + 10)
    returnCode, floor = sim.simxGetObjectHandle(clientID, "ResizableFloor_5_25", sim.simx_opmode_oneshot_wait)
  
  # Prosseguindo com as leituras
  returnCode, string_range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_buffer)
  raw_range_data = sim.simxUnpackFloats(string_range_data)
  
  scan_range = 180 * np.pi / 180
  step_size = 2 * np.pi / 1024
  laser_data = format_laser_data(raw_range_data, scan_range, step_size)
  
  returnCode, pos = sim.simxGetObjectPosition(clientID, robotHandle, -1, sim.simx_opmode_oneshot_wait)        
  
  # Dados do Pioneer
  L = 0.381   # Metros
  r = 0.0975  # Metros

  t = 0
  startTime = time.time()
  lastTime = startTime

  fig = plt.figure(figsize=(6, 6), dpi=100)
  ax = fig.add_subplot(111, aspect="equal")

  while t < 20:
    now = time.time()
    dt = now - lastTime

    # Fazendo leitura do laser       
    returnCode, string_range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_buffer)
    raw_range_data = sim.simxUnpackFloats(string_range_data)
    laser_data = format_laser_data(raw_range_data, scan_range, step_size)

    returnCode = 1
    while returnCode != 0:
      # Encontra a posição do robô em relação ao mundo
      returnCode, robot_t = sim.simxGetObjectPosition(clientID, robotHandle, -1, sim.simx_opmode_streaming)

      # Encontra a rotação relativa do chão em relação ao robô
      returnCode, floor_r = sim.simxGetObjectOrientation(clientID, floor, robotHandle, sim.simx_opmode_streaming)

    # Cria a matriz de transformação
    floor_m = create_matrix(robot_t, Rz(floor_r[2]))

    # Desenha os pontos que o laser do robô vê transladados pela matriz criada
    draw_laser_data_persistent(ax, laser_data, floor_m)

    # Plota a posição atual do robo
    plt.plot(robot_t[0], robot_t[1], marker="o", markersize=2, color="black")
    
    # Velocidade básica (linear, angular)
    v = 0
    w = np.deg2rad(0)      

    frente = int(len(laser_data)/2)
    lado_direito = int(len(laser_data)*1/4)
    lado_esquerdo = int(len(laser_data)*3/4)
    
    if laser_data[frente, 1] > 2:
      v = 0.5
      w = 0
    elif laser_data[lado_direito, 1] > 2:
      v = 0
      w = np.deg2rad(-30)
    elif laser_data[lado_esquerdo, 1] > 2:
      v = 0
      w = np.deg2rad(30)
    
    # Isso é o modelo cinemático, estudaremos detalhadamente depois!
    wl = v/r - (w*L)/(2*r)
    wr = v/r + (w*L)/(2*r)
    
    # Enviando velocidades
    sim.simxSetJointTargetVelocity(clientID, l_wheel, wl, sim.simx_opmode_streaming + 5)
    sim.simxSetJointTargetVelocity(clientID, r_wheel, wr, sim.simx_opmode_streaming + 5)        

    t = t + dt  
    lastTime = now

  # Salvando o mapa
  plt.savefig("room_map.png")

  # Parando o robô    
  sim.simxSetJointTargetVelocity(clientID, r_wheel, 0, sim.simx_opmode_oneshot_wait)
  sim.simxSetJointTargetVelocity(clientID, l_wheel, 0, sim.simx_opmode_oneshot_wait)        
    
  sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)         
  sim.simxFinish(clientID)
else:
  print ('Failed connecting to remote API server')
    
print ('Program ended')